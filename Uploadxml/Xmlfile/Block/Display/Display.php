<?php
/**
 * Copyright © 2015 Uploadxml . All rights reserved.
 */
namespace Uploadxml\Xmlfile\Block\Display;
class Display extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}

	public function display()
	{
		return __('Hello World');
	}
}


