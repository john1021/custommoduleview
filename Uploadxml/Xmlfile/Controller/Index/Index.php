<?php
/**
 *
 * Copyright © 2015 Uploadxmlcommerce. All rights reserved.
 */
namespace Uploadxml\Xmlfile\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
	/* Starts Process For Base URL */
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	/* Finish Process For Base URL */
		
	if(isset($_FILES["fileToUpload"]["name"]))	{	
		
	$filename    = $_FILES["fileToUpload"]["name"];
	$filenamexml=$filename;
	$target_dir  = "uploads/";
	$file_path   ="uploads/"."$filename";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
  



   $filename=$filename;

	 /* Starts Process as Per 356 & 60 Format */
	  @$xml = simplexml_load_file("uploads/"."$filename");
  
  

  
  
 if($filename==$filenamexml) {
 
    for($i=1;$i<=1;$i++){
		foreach($xml->children() as $book)
		{
		 
			 echo"<BR>waid==".$waid[$i]=$book->waid;  
			 echo"<BR>LEVEL1ID==".$LEVEL1ID[$i]=$book->LEVEL1ID;
			 echo"<BR>LEVEL2ID==".$LEVEL2ID[$i]=$book->LEVEL2ID; 
			 echo"<BR>LEVEL3ID==".$LEVEL3ID[$i]=$book->LEVEL3ID;
			 
			  echo"<BR>LEVEL2ID==".$DUMMY[$i]=$book->DUMMY; 
			 echo"<BR>LEVEL3ID==".$DUMMYHIS[$i]=$book->DUMMYHIS;
			 
			 if(isset($NOTES[$i]) && !empty($NOTES[$i])) {
			 echo"<BR>NOTES==".$NOTES[$i]=$book->NOTES; 
			 }else{ 
			 echo"<BR>NOTES==".$NOTES[$i]=""; 
			 }
			 
			 if(isset($CASENOTES[$i]) && !empty($CASENOTES[$i])) {
			 echo"<BR>CASENOTES==".$CASENOTES[$i]=$book->CASENOTES; 
			 }else{ 
			 echo"<BR>CASENOTES==".$CASENOTES[$i]=""; 
			 }
			 
			 

			 echo"<BR>State==".$State[$i]=$book->State;
			 echo"<BR>CITE==".$CITE[$i]=$book->CITE;
			 echo"<BR>HtmlSecmain==".$HtmlSecmain[$i]=$book->HtmlSecmain;
			 
			 
		/*Starts Insert Query */
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('tblsectiondetails'); //gives table name with prefix
        /*Finish Insert Query */
  
				  
			 
	
	/*Starts without Notes & CaseNotes */
	//echo"<BR>===".$sql="INSERT INTO tblsectiondetails (WAID, LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID, SECTION1, CITE, HtmlSecmain, NOTES, CASENOTES, SectionHistory, Book_ID) 
	//VALUES($waid[$i], $LEVEL1ID[$i], $LEVEL2ID[$i], $LEVEL3ID[$i], NULL, NULL, \"$DUMMY[$i]\", '$CITE[$i]', \"$HtmlSecmain[$i]\", '', '', \"$DUMMYHIS[$i]\", 3000)";
    /*Finish without Notes & CaseNotes */
	
	
	/*Starts with Notes & CaseNotes */
	$sql="INSERT INTO $tableName (WAID, LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID, SECTION1, CITE, HtmlSecmain, NOTES, CASENOTES, SectionHistory, Book_ID) 
	VALUES($waid[$i], $LEVEL1ID[$i], $LEVEL2ID[$i], $LEVEL3ID[$i], NULL, NULL, \"$DUMMY[$i]\", '$CITE[$i]', \"$HtmlSecmain[$i]\", \"$NOTES[$i]\", \"$CASENOTES[$i]\", \"$DUMMYHIS[$i]\", 3000)";
    /*Finish with Notes & CaseNotes */
	$result = $connection->query($sql);
	      /*  if($result) {
            $this->resultPage = $this->resultPageFactory->create();  
		    return $this->resultPage;
            } else {
            $this->resultPage = $this->resultPageFactory->create();  
		    return $this->resultPage;
            }*/
			
			
		}
		
    }
  }  
}
        $this->resultPage = $this->resultPageFactory->create();  
		return $this->resultPage;
        
    }
}
